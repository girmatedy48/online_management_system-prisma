/*
  Warnings:

  - You are about to drop the column `video` on the `Videos` table. All the data in the column will be lost.
  - Added the required column `video_path` to the `Videos` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Videos" DROP COLUMN "video",
ADD COLUMN     "video_path" TEXT NOT NULL;
