/*
  Warnings:

  - You are about to drop the column `courseName` on the `Course` table. All the data in the column will be lost.
  - You are about to drop the column `firstName` on the `Course` table. All the data in the column will be lost.
  - You are about to drop the column `lastName` on the `Course` table. All the data in the column will be lost.
  - You are about to drop the column `social` on the `Course` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[name]` on the table `Course` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `courseDetails` to the `Course` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Course` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "Course_courseName_key";

-- AlterTable
ALTER TABLE "Course" DROP COLUMN "courseName",
DROP COLUMN "firstName",
DROP COLUMN "lastName",
DROP COLUMN "social",
ADD COLUMN     "courseDetails" TEXT NOT NULL,
ADD COLUMN     "name" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Course_name_key" ON "Course"("name");
