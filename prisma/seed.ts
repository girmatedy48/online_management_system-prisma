import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
const {user}=require('./User');

// async function seedUser() {
//     for (const users of user){
//         await prisma.userInfo.create(
//             {
//                 data:users
//             }
//         )
//     }
// }
async function seedUser(){
    await prisma.userInfo.createMany({
    data:user
    })
}
//execure seedUsers

seedUser().catch((e)=>{
    console.log(e);
    process.exit(1)
})
.finally(()=>{
    console.log("users seeded")
    prisma.$disconnect();

})