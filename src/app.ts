import express, { NextFunction, Request, Response } from 'express'
import errorhandler from '../utils/errorHandle'
import dotenv from 'dotenv'
import auth from '../Routes/Authorization'
import course from '../Routes/Course'
import Test from '../Routes/CourseTest'
import TestR from '../Routes/TestResult'
import users from '../Routes/User'
import enroll from '../Routes/CourseEnrollement'
import videos from '../Routes/VideoUploading'
const app=express()

dotenv.config()
//middleware
app.use(errorhandler)
app.use(express.json())

//error handler

app.use((err:any,_req:any,res:any,next:NextFunction)=>{
    res.status(err.statusCode || 500).json({
        err:true,
        message:err.message
    })
})

//middle ware to upload image

app.use('/uploads',express.static('uploads'))
app.use('/videos',express.static('videos'))
//Routes
// test
const data=[
    {userId:1,courseId:1,result:10},
    {userId:2,courseId:2,result:20},
]
app.get('/',(req,res)=>{
    res.status(200).json({
        err:false,
        message:"this is Tedy Room",
        data:data
    })
})
app.use('/api/auth',auth)
app.use('/api/user',users)
app.use('/api/course',course)
app.use('/api/enroll',enroll)
app.use('/api/test',Test)
app.use('/api/testResult',TestR)
app.use('/api/videos',videos)


export default app;