
import app from "./app";

//listen our app
const PORT =process.env.PORT || 4000;

app.listen(PORT, ()=>console.log(`app running on http://localhost:${PORT}`))

export default app;