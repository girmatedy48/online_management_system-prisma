import request from 'supertest'
import app from '../src/app'

describe("check our end point", () => {
  test("status endpoint should return 200", async () => {
    const resp = await request(app).get('/')  
    expect(resp.statusCode).toEqual(200)
    expect(resp.type).toContain('application/json');
  })
})

//User Registrationit
let userId;
let token;
let courseId;

describe("Test case for Authorization Including Registration and login",()=>{
  test('lets register user',async()=>{
  
    const res=await request(app).post('/api/auth/register').send({
      email: `red@gmail.com`,
      password: 'password',
      firstName: '123',
      lastName:'yohana',
      role:"MEMBERS"
    })

    userId=res.body.data.id;
    expect(res.statusCode).toEqual(201)
    // expect(res.type).toContain('application/json')
  })

  test('Return bad request when body is not passed',async()=>{
    const res=await request(app).post('/api/auth/register').send({})
    expect(res.statusCode).toEqual(422)
  })

  it("returns 200 status code when submiting username and password",async()=>{
    const res=await request(app).post('/api/auth/login').send({
      email:"tedJoo@gmail.com",
      password:"password"
    })  
    token=res.body.data.accessToken;
    expect(res.statusCode).toBe(200)
    expect({message:"Successfully logged in"})
  })

  //returns json return
  it('returns 400 when email of user is not found',async()=>{
    const res=await request(app).post('/api/auth/login').send({
      email:"ted2@gmail.com",
      password:"password"
    })
    expect(res.type).toContain('application/json');
    expect(res.statusCode).toBe(400)
  })

 


  //Course test
  describe('Test Case for course',()=>{
    test('register the course and get 201',async()=>{
        // let num1=Math.floor(Math.random() * 10)
        const resp=await request(app).post('/api/course').send({
            name:"course1",
            courseDetails:"This Is best course"
        })
       .set('authorization', `Bearer ${token}`);

        courseId=resp.body.data.id 
        expect(resp.statusCode).toBe(201)
        expect(resp.type).toContain('application/json')
        expect({message:"Successfully Saved"})
    })
    //check uprocessible entry
    test('return 400 when course is not found',async()=>{
            const result=await request(app).post('/api/course/:sa')
            expect(result.statusCode).toBe(404)
    })

    test('should return json format',async()=>{
        const result=await request(app).get('/api/course')
       .set('authorization', `Bearer ${token}`);

        expect(result.type).toContain('application/json')
        expect(result.statusCode).toBe(200)
        expect({message:"Successfully fetched"})
    })
    //delete course

    test('return course by id',async()=>{
      const result=await request(app).get(`/api/course/${courseId}`)
      .set('authorization', `Bearer ${token}`);
      expect(result.statusCode).toBe(200)
      expect(result.type).toContain('application/json')
      expect({message:"data is successfully fetched"})
    })
    
    test('status code must be 400 when no parameter is passed for the endpoint',async()=>{
      const result=await request(app).get('/api/course/:id')
      .set('authorization', `Bearer ${token}`);
      expect(result.statusCode).toBe(404)
    })

    test('course should have to be updated by its id',async()=>{
      const num=Math.floor(Math.random() * 10)
      const result=await request(app).put(`/api/course/updateCourse/${courseId}`).send({
        name:`Modern${num} Jave`,
        courseDetails:"best modern java book"
      })
      .set('authorization', `Bearer ${token}`);
      expect(result.statusCode).toBe(200)
      expect(result.type).toContain('application/json')
    })
    
    test('when our course params is passed but deleted before,so status code is going to be 500',async()=>{
        const result=await request(app).delete('/api/course/deleteCourse/:id')
        .set('authorization', `Bearer ${token}`)
        expect(result.statusCode).toBe(500)
    })

    //finally delete course
    test('delete course by id',async()=>{
      const result=await request(app).delete(`/api/course/deleteCourse/${courseId}`)
      .set('authorization', `Bearer ${token}`);
      expect(result.statusCode).toBe(200)
      expect({message:"Successfully Deleted"})
    })
  }
)

//finally delete the user
test('delete user',async()=>{
  const res=await request(app).delete(`/api/user/deleteUser/${userId}`)
  .set('authorization', `Bearer ${token}`);
  expect(res.statusCode).toEqual(200)
})

})



