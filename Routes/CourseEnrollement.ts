import express from 'express'

import { deleteUserRegistered, enroll, get_enrolled_users, get_enrollement } from '../Controllers/enrollement'
import { authorizeAdmin, verifyToken } from '../utils/veriffyToken';

const router=express.Router()

//create enrollement

router.post('/',verifyToken,enroll)
//delete the posr
router.get('/',authorizeAdmin,get_enrollement);
//get user coursed
router.get('/user/course',verifyToken,get_enrolled_users);

//delete the use
router.delete('/user/course/:courseId',verifyToken,deleteUserRegistered)
//for admin
router.delete('/user/:id/course/:courseId',authorizeAdmin,deleteUserRegistered)



export default router