import express from 'express';
import {addTest,deleteTest,getTest, getTestById, updateTest} from '../Controllers/coursetest';
import { authorizeAdmin } from '../utils/veriffyToken';
const router=express.Router();

//post test
router.post('/',authorizeAdmin,addTest);

//update the test
router.delete('/:id',authorizeAdmin,deleteTest);
//delete the test
router.get('/',authorizeAdmin,getTest);
// the update test

router.put('/test/:id',authorizeAdmin,updateTest);
//get the test
router.get('/test/:id',authorizeAdmin,getTestById);
export default router;