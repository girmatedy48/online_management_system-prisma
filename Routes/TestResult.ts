import express from 'express'
import { createTest,deleteTestResult,getAllTestResult,getTestAggregation, updateTestResult, userResult } from '../Controllers/testResult'
import { authorizeAdmin, verifyToken } from '../utils/veriffyToken'
const router=express.Router()

router.post('/',authorizeAdmin,createTest),
router.get('/result',authorizeAdmin,getAllTestResult)
router.get('/',authorizeAdmin,getTestAggregation)
router.get('/user/:id/result',authorizeAdmin,userResult)
router.get('/user/result',verifyToken,userResult)
router.patch('/:id',authorizeAdmin,updateTestResult)
router.delete('/:id',authorizeAdmin,deleteTestResult)

export default router
