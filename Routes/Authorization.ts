import express from 'express'
// import register from '../Controllers/authorization'
import {login,register} from '../Controllers/authorization'
import { profileImage } from '../utils/uploadingImage'
// import profileImage  from '../utils/uploadingImage'
const router=express.Router()

//register user
router.post('/register',profileImage.single('profileImage'),register)
//authorize user
router.post ('/login',login)

export default router