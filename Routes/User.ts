//delete user
// router.delete('/deleteUser/:id',deleteUser)
import express from 'express'
import { deleteUser, getUser, getUserById, getUserByRole, updateUser } from '../Controllers/user'
import { authorizeAdmin, verifyToken } from '../utils/veriffyToken'
const router=express.Router()

//get user by id
router.get('/:id',authorizeAdmin,getUserById)

router.get('/',authorizeAdmin,getUser)

router.delete('/deleteUser/:id',authorizeAdmin,deleteUser)
//update user by id
router.put('/updateUser',verifyToken,updateUser)
//admin can update

export default router