import express from 'express'
import { deleteCourse, getCourse, getCourseById, saveCourse, updateCourse } from '../Controllers/course'
import   {verifyToken,authorizeAdmin}  from '../utils/veriffyToken'
const router=express.Router()
// const router=express.Router()
router.post('/',authorizeAdmin,saveCourse)
router.get('/',verifyToken,getCourse )
router.get('/:id',verifyToken,getCourseById )
router.put('/updateCourse/:id',authorizeAdmin,updateCourse)
router.delete('/deleteCourse/:id',authorizeAdmin,deleteCourse)
export default router