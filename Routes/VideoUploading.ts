import express from 'express'
import { getVideos, uploadVideos } from '../Controllers/video';
import { videoPofile } from '../utils/uploadingVideos';

import { authorizeAdmin, verifyToken } from '../utils/veriffyToken';
const router=express.Router()

//upload videos
router.post('/upload',authorizeAdmin,videoPofile.single('videopath'),uploadVideos)

//get all videos

router.get('/',verifyToken,getVideos)
export default router;