interface Register {
    status?: number
    name?: string
    message?: string
    stack?: string
  }
  
  export default Register;
  