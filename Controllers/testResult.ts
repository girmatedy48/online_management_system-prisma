import { PrismaClient } from "@prisma/client";
import { createTestResultValidation } from "../utils/validation";
import { get_enrolled_users } from "./enrollement";

const prisma=new PrismaClient();

export const createTest=async(req:any,res:any,next:any)=>{
    const user=req.user.user
    const data={
        studentEmail:req.body.studentEmail,
        testId:req.body.testId,
        courseId:req.body.courseId,
        teacherEmail:user.email,
        score:req.body.score,
    }
    try {
        const result= await createTestResultValidation.validateAsync(data);

        const studentInfo=await prisma.user.findUnique({
            where:{
                email:result.studentEmail,
            }
        })

        
        if(!studentInfo){
            return res.status(400).json({
                err:true,
                message:"Student Information is not found..."
            })
        }

        const userEnrollementCheckup=await prisma.courseEnrollment.findMany({
            where:{
                userId:studentInfo?.id,
                courseOfId:result.courseId
            }
        })

        if(userEnrollementCheckup.length ===0){
            return res.status(400).json({
                err:true,
                message:"student should have to enroll to this course before getting test result"
            })
        }

        const testResult= await prisma.testResult.create({
            data:{
                student_id:{
                    connect:{
                        email:result.studentEmail
                    }
                },
                teacher_id:{
                    connect:{
                        email:result.teacherEmail
                    }
                },
                course_id:{
                    connect:{
                        id:result.courseId
                    }
                },
                test:{
                    connect:{
                        id:result.testId
                    }
                },
                score:result.score
            },
            select:{
                student_id:true,
                teacher_id:true,
                course_id:true,
                score:true,
            }
        })

        res.status(200).json({
            err:false,
            message:"Result successfully saved",
            data:testResult
        })

    } catch (error:any) {
        if(error.name == "ValidationError" || error.name == "PrismaClientValidationError"){
          const message=error.message.toString().replace(/[^a-zA-Z ]/g, "")
          return  res.status(422).json({
                err:true,
                err_message:message,
                message:"Something went wrong"
            })
        }
        res.status(500).json({
            err:true,
            message:"Something while registering",
        })
    }
}

export const getTestAggregation=async(req:any,res:any,next:any)=>{
        
        try {
        const result =await prisma.testResult.groupBy(
            {
               by:['courseId','studentId','teacherId'],
               _sum:{score:true}
            
            }
        )
        // console.log("res",result)
        res.status(200).json({
            err:false,
            message:"Result successfully saved",
            data:result
        })
    } catch (error) {
        console.log(error)
    }
}

//get all Test Resultse

export const getAllTestResult=async(req:any,res:any)=>{
    try {
        const limit=Number(req.query.limit)  || 4;
        let perpage =Number(req.query.page) -1 || 0;
        if(perpage < 0) perpage=0;
        let search= Number(req.query.search) || 0;
        const skip=Number(limit * perpage);
        let testResultCount;
        const testResult=await prisma.testResult.findMany({
            where:{
                score:{
                    gte:search
                }
            },
            skip:skip,
            take:limit
        })

        if(search !== 0){
            testResultCount= await prisma.testResult.count({
                where:{
                    score:{
                        gte:search
                    }
                }
            })
        }
        else{
            testResultCount=await prisma.testResult.count()
        }

        res.status(200).json({
            err:false,
            message:"Data successfully fetched",
            count:testResultCount,
            testResult:testResult
        })

    } catch (error:any) {
        res.status(404).json({
            err:true,
            errorMessage:error,
            message:"Something wen't wrong"
        })
    }
}

//get single user result test

export const userResult=async(req:any,res:any,)=>{

    const userId=req.params.id || req.user.user.id;
    const score=Number(req.query.score) || 0;
    const limit=Number(req.query.limit)  || 4;
    let perpage =Number(req.query.page) -1 || 0;
    let  resultCount
    if(perpage < 0) perpage=0;
    try {
        const result=await prisma.testResult.findMany({
            where:{
                studentId:userId,
                score:{
                    gte:score
                }
            },
            skip:perpage * limit,
            take:limit

        })

        if(!result){
            return res.status(404).json({
                err:true,
                message:"result is not found with student id"
            })
        }

        if(score !== 0){
             resultCount= await prisma.testResult.count({
                where:{
                    studentId:userId,
                    score:{
                        gte:score
                    }
                }
            }
            )
        }
        else{
            resultCount=await prisma.testResult.count({
                where:{
                    studentId:userId
                }
            })
        }

        const data={
            userId:userId,
            totalResult:resultCount,
            result:result.map((data)=>{
                return {
                    courseId:data.courseId,
                    score:data.score,
                    teacher:data.teacherId,
                    testId:data.testId,
                }
            })
        }

        res.status(200).json({
            err:false,
            message:"data successfully fetched !!!",
            data:data
        })

    } catch (error) {
        console.log(error)
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}

//update test result
export const updateTestResult=async(req:any,res:any)=>{
    const id=req.params.id;
    try {
        
        if(!id){
            return res.status(400).json({
                err:true,
                message:"id is required"
            })
        }

        const result=await prisma.testResult.update({
            where:{
                id:req.params.id
            },
            data:{
                score:req.body.score
            }
        })
        res.status(200).json({
            err:false,
            message:"data successfully updated",
            data:result
        })

    } catch (error) {
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}

//delet the test resut;

export const deleteTestResult=async(req:any,res:any)=>{
    const id=req.params.id;
    try {
        //find if the is deleted before or not
        const testResult=await prisma.testResult.findUnique({
            where:{
                id:id
            }
        })
        if(!testResult){
            return res.status(404).json({
                err:false,
                message:"test result is not found"
            })
        }

        await prisma.testResult.delete({
            where:{
                id:req.params.id
            }
        })
        res.status(200).json({
            err:false,
            message:"data successfully deleted",
        })

    } catch (error) {
        console.log(error)
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}