import { PrismaClient } from "@prisma/client";
import { courseEnrollment, deleteUserEnrollement,updateUserEnrollementValidation } from "../utils/validation";

const prisma=new PrismaClient()

export const enroll =async(req:any,res:any)=>{
    const userId=req.user.user.id;
    console.log("user",userId)  
    const role=req.user.user.role;
    const courseId=req.body.courseId;
    const data={
        userId,
        courseId,
        role
    }
    try {
        const result=await courseEnrollment.validateAsync(data)
        const enroll_course=await prisma.courseEnrollment.create({
            data:{
                courseId:{
                    connect:{
                     id:result.courseId
                    }
                },
                courseUserId:{
                    connect:{
                        id:result.userId
                    }
                },
                role:result.role
            }
        })

        res.status(200).json({
            err:false,
            message:"Enrollement successfully registered",
            data:enroll_course
        })
    } catch (error:any) {
        // console.log(error)
        if(error.name == 'ValidationError'){
            const message=error.message.replace(/[^a-zA-Z ]/g, "")
            return res.status(422).json({
                err:true,
                message:"Something wen't wrong !!!",
                err_message:message,
            })
        }
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}

//get all enrollement of course

export const get_enrollement=async(req:any,res:any)=>{
        const limit=Number(req.query.limit)  || 4;
        let perpage =Number(req.query.page) -1 || 0;
        if(perpage < 0) perpage=0;
        
        const skip=Number(limit * perpage);
    try {
        const enrolls = await prisma.courseEnrollment.findMany({
            skip:skip,
            take:limit
        })
        console.log("enrolls",enrolls)
        
        const totalEnrollement=await prisma.courseEnrollment.count()
        
        res.status(200).json({
            err:false,
            message:"enrollement fetched successfully",
            totalEnrollement:totalEnrollement,
            data:enrolls
        })
 
    } catch (error) {
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })      
    }
}

//get users enrolled with course
export const get_enrolled_users=async(req:any,res:any)=>{
    const userId=req.user.user.id;

    try {
        if(userId == ""){
            return res.status(404).json({
                err:true,
                message:"userId is required"
            })
        }
        const enrolled_users=await prisma.courseEnrollment.findMany({
           where:{
               userId:userId
           },
           include:{
                courseId:{
                    select:{
                        name:true,
                        courseDetails:true
                    }
                }
           }
        })
        const countUserCourses=await prisma.courseEnrollment.count({
            where:{
                userId:userId
            }
        })
        
        if(!enrolled_users){
            return res.status.json({
                err:true,
                message:"we couldn't found the student"
            })
        }
        const data={
            totalCourses:countUserCourses,
            userId:userId,
            courses:enrolled_users.map((data)=>{
              return{
                courseOfId:data.courseOfId,
                createdAt:data.createdAt,
                role:data.role,
                updatedAt:data.updatedAt,
                DetailofTheCourse:data.courseId,
              } 
            })
        }
        res.status(200).json({
            err:false,
            message:"data is successfully fetched",
            data:data
        })
    } catch (error) {
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}

//delete the user enrolled

export const deleteUserRegistered=async(req:any,res:any)=>{
    const id=req.user.user.id || req.params.id;
    const courseId=req.params.courseId || "";

    const data={
        id,
        courseId
    }
    try {
        if(id == ""){
            return res.status(404).json({
                err:true,
                message:"userId is required"
            })
        }
        const result=await deleteUserEnrollement.validateAsync(data)
            const deleteUser=await prisma.courseEnrollment.deleteMany({
                where:{
                    userId:result.id,
                    courseOfId:result.courseId
                }
            })
            if(!deleteUser){
                return res.status(404).json({
                    err:true,
                    message:"user is not found"
                })
            }
            res.status(200).json({
                err:false,
                message:"User course is successfully deleted"
            })
    } catch (error:any) {
        // console.log(error)
        if(error.name == 'ValidationError'){
            const message=error.message.replace(/[^a-ZA-Z]/g,"")
            return res.status(422).json({
                err:true,
                message:"validation error",
                err_message:message
            })
        }
        res.status(404).json({
            err:true,
            message:"Something wen't wrong !!"
        })
    }
}

