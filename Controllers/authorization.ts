import { PrismaClient } from "@prisma/client"
import bcrypt from 'bcrypt'
import { NextFunction, Request, Response } from "express"
import * as jwt from 'jsonwebtoken'
const prisma =new PrismaClient()
import {registerSchema,loginSchema}from '../utils/validation'

export const register=async(req:Request,res:Response,next:NextFunction)=>{
    const filename=req.file?.path;
    const data={
        email:req.body.email,
        password:req.body.password,
        lastName:req.body.lastName,
        firstName:req.body.firstName,
        role:req.body.role,
        profilePicture:filename
    }
    try {
        const result= await registerSchema.validateAsync(data)
        const newUser=await prisma.user.create({
            data:{
                ...result,
                password:await bcrypt.hash(result.password,10),
                
            }
        })
        const {password ,...user}=newUser
        res.status(201).json({
            err:false,
            message:"Successfully Registered",
            data:user
        })
    } catch (error:any) {
        if(error.name == 'ValidationError'){
            const message=error.message.toString().replace(/[^a-zA-Z ]/g, "")
            return  res.status(422).json({
                  err:true,
                  message:"Error in logging in",
                  err_message:message,
              })
          }
               res.status(500).json({
                  err:true,
                  errorstatus:error,
                message:"Something went wrong"
          })
       
}
}

//authorize user
 export const login=async(req:Request,res:Response,next:NextFunction)=>{

   const secreteNode:string|any=process.env.SECRET
   
   try {
        const Result= await loginSchema.validateAsync(req.body)

        const user=await prisma.user.findUnique({ 
            where:{
                email:Result.email
            }
        })
        // check ther iser
        if(!user){
            return res.status(400).json({
                error:true,
                message:"User not found"
            })
        }
        //compare user password
            if(user && bcrypt.compareSync(Result.password,user.password)){
                const {password, ...others}=user;
                const accessToken=jwt.sign({
                    user:others,
                },
                secreteNode
                ,{
                expiresIn:'5d'
            }
            )

            return res.status(200).json({
                error:false,
                message:"Successfully logged in",
                data:{
                    userData:others,
                    accessToken
                }
            })
        }
        else{
            return res.status(401).json({
                error:true,
                message:"Invalid credentials"
            })
        } 
        
    } catch (error:any) {
        if(error.name == 'ValidationError'){
            const message=error.message.toString().replace(/[^a-zA-Z ]/g, "")
          return  res.status(422).json({
                err:true,
                message:"Error in logging in",
                err_message:message,
            })
        }
        else{
            return res.status(401).json({
                err:true,
                errorstatus:error.name,
                message:"user is not authorized",
            })
        }
        // next(error)
    }
 }

 export default {register,login}
//  export default login