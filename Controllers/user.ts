import { PrismaClient } from "@prisma/client";
import { NextFunction } from "express";

const prisma=new PrismaClient()

export const getUser=async(req:any,res:any,next:any)=>{
    try {
        const limit=Number(req.query.limit)  || 4;
        let perpage =Number(req.query.page) -1 || 0;
        if(perpage < 0) perpage=0;
        const search= req.query.search || "";
        const skip=Number(limit * perpage);
        let countUsers;
        const user=await prisma.user.findMany({
            where:{
                firstName:{
                    startsWith:search,
                    contains:search,
                    endsWith:search,
                    mode:"insensitive"
                },
            },
            skip:skip,
            take:limit,
            select:{
                id:true,
                firstName:true,
                lastName:true,
                email:true,
                role:true,
                social:true,
                profilePicture:true
            }
            
        },
        );
        //count users
        if(search !== ""){
            countUsers=await prisma.user.count({
                where:{
                    firstName:{
                        contains:search,
                        startsWith:search,
                        endsWith:search,
                        mode:"insensitive"
                    }
                }
            })
        }
        else{
            countUsers=await prisma.user.count()
        }
        
        if(user.length === 0){
          return  res.status(404).json({
                err:true,
                message:"sorry please try again",
            })
        }
        res.status(200).json({
            err:false,
            message:"data successfully fetched",
            count:countUsers,
            data:user,

        })
    } catch (error) {
        // console.log(error)
        res.status(500).json({
            err:true,
            message:"something wen't wrong"
        })
    }
}


export const getUserById=async(req:any,res:any,next:any)=>{
    try {
        const id=req.params.id;
        const user=await prisma.user.findUnique({
where:{
    id:id
},
select:{
    id:true,
    firstName:true,
    email:true,
    lastName:true
}
})
        if (!user){
            return res.status(404).json({
                error:true,
                message:"user is not found",
            })
        }


    res.status(200).json({
        error:false,
        message:"Successfully fetched",
        data:user
    })
}
    catch (error) {
        return res.status(422).json({
            error:true,
            message:"Something wen't wrong",
        })
    }
}

//delete the user
export const deleteUser=async(req:any,res:any,next:any)=>{
    try {
        const id=req.params.id;
       const ted =await prisma.user.delete({
            where:{
                id:id
            }
        })
        // console.log(ted)
        res.status(200).json({  
            error:false,
            message:"Successfully Deleted",
        })
    } catch (error) {
        // console.log(error)
        res.status(500).json({
            err:true,
            message:"something wen't wrong"
        })
    }
}
//update the user
export const updateUser=async(req:any,res:any,next:any)=>{
    try {
        const id=req.user.user.id || req.params.id;
        const data=req.body;
        if(!data.firstName || !data.lastName || !data.email){
            return res.status(422).json({
                error:true,
                message:"Please provide all the fields"
            })
        }

        const user=await prisma.user.update({
            where:{
                id:id
            },
            data:data
        })
        res.status(200).json({
            error:false,
            message:"Successfully Updated",
            data:user
        })

    } catch (error:any) {
        res.status(500).json({
            err:true,
            err_message:error.message,
            message:"something wen't wrong"
        })
    }
}
//get user by role
export const getUserByRole=async(req:any,res:any,next:NextFunction)=>{
    try {
        const role=req.params.role;
        const user=await prisma.user.findMany({
            where:{
                role:role
            }
        })

        res.status(200).json({
            error:false,
            message:"Successfully fetched",
            data:user})

    } catch (error) {
        res.status(500).json({
            err:true,
            message:"something wen't wrong"
        })
    }
}

// user enroll aggregation

export const getResult=async(req:any,res:any,next:NextFunction)=>{
    try {
        const userId=req.params.id;
        const result=await prisma.testResult.aggregate({
            where:{
                studentId:userId
            },
            _avg:{score:true},
            _min:{score:true},
            _max:{score:true}
        })

        res.status(200).json({
            error:false,
            message:"Successfully fetched",
            data:result
        })

    } catch (error) {
        res.status(500).json({
            err:true,
            message:"something wen't wrong"
        })
    }
}
