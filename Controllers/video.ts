import express, { NextFunction, Request, Response } from  'express'
import { PrismaClient } from '@prisma/client'
const prisma =new PrismaClient()

const router=express.Router()

export const uploadVideos=async(req:any,res:Response,next:NextFunction)=>{
    const filename=req.file?.path;
    
    // console.log("filename",filename)
    const user = req.user.user.id;
    const videopath=req.file?.path
    try {
        const newVideos = await prisma.videos.create({
            data:{
                userId:user,
                video_path:videopath,
                descriptions:req.body.desc,
            }
        })
        res.status(200).json({
            err:false,
            message:"Videos uploaded successfully",
            data:newVideos
        })
    } catch (error) {
        res.status(500).json({
            err:true,   
            message:"Something went wrong"
        })  
    }
}

export const getVideos=async(req:any,res:Response,next:NextFunction)=>{
//retrive all videos
//retrive all videos with user information
    try{
        const videos = await prisma.videos.findMany({
            include:{
                videos:{
                    select:{
                        email:true,
                        firstName:true,
                        lastName:true,
                        profilePicture:true
                    }
                }
            }
        })
        res.status(201).json({
            err:false,
            message:"Videos fetched successfully",
            data:videos
        })
    }
    catch(error){
        res.status(500).json({
            err:true,
            message:"Something went wrong"
        })
    }
}

export default router;