import {PrismaClient}from'@prisma/client'
import { testSchema } from '../utils/validation';
const prisma=new PrismaClient()

export const addTest=async (req:any,res:any)=>{
    
    
    try {
        const result=await testSchema.validateAsync(req.body)        
        
        const new_date=new Date(result.date)
        
        const test=await prisma.test.create({
            data:{
                name:result.name,
                date:new_date,
                courseofId:{
                    connect:{
                        id:result.courseId 
                    }
                }
            }
        });


        res.status(200).json({
            error:false,
            message:"Successfully Added",
            data:test
        })
    } catch (error:any) {
        if(error.name==='ValidationError'){
            const message=error.message.replace(/[^a-zA-Z ]/g,"")
            return res.status(422).json({
                err:true,
                message:"something went wrong !",
                errMessage:message
            })
        }
    }
}

//delete the test
export const deleteTest=async (req:any,res:any)=>{
    try {
        const testId=req.params.id;
       await prisma.test.delete({
        where:{
            id:testId
        }
       })
       res.status(200).json({
            error:false,
            message:"Successfully Deleted",
       })
    } catch (error) {
        res.status(500).json({
            error:true,
            message:"something went wrong"
        })
    }
}
//update the test
export const updateTest=async (req:any,res:any)=>{
    try {
        const data=req.body;
        const idTest=req.params.id;
        const test=await prisma.test.update({
            where:{id:idTest},
            data:data
        })
        res.status(200).json({
            error:false,
            message:"Successfully Updated",
            data:test
        })
    } catch (error) {
        res.status(500).json({
            error:true,
            message:"something went wrong"
        })
    }
}

//get the tastes

export const getTest=async (req:any,res:any)=>{

    const limit=Number(req.query.limit) || 5;
    const page=Number(req.query.page) -1 || 0;
    const skip=page * limit;

    try {
        const test=await prisma.test.findMany({
             orderBy:{
                id: "desc",
            },
            include:{
                courseofId:true
            },
            skip:skip,
            take:limit
        })
        res.status(200).json({
            error:false,
            message:"Successfully Fetched",
            data:test
        })
    } catch (error) {
        res.status(500).json({
            error:true,
            message:"something went wrong"
        })
    }
}

export const getTestById=async (req:any,res:any)=>{
    const id=req.params.id;
 
    try {
        const test=await prisma.test.findMany({
            where:{
                id:id
            },
            include:{
                courseofId:true
            }
        })

        if(test.length <= 0){
            return res.status(400).json({
                error:true,
                message:"Test not found",
                err_message:"Please check your test id"
            })
        }

        res.status(200).json({
            error:false,
            message:"Successfully Fetched",
            data:test
        })
    } catch (error) {
        res.status(500).json({
            error:true,
            message:"something went wrong"
        })
    }
}

export default {addTest,deleteTest}