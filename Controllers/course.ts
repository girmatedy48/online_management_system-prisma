import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()
import { NextFunction, Request, Response } from 'express'

import { registerCourseSchema } from '../utils/validation'
// import test from './test'
// import {verifyToken} from '../utils/veriffyToken'
//save cour
export const saveCourse=async(req:Request,res:Response,next:NextFunction)=>{
    
    
    try {
        const result= await registerCourseSchema.validateAsync(req.body)
        const courser=await prisma.course.create({
            data:{
                name:result.name,
                courseDetails:result.courseDetails           
            }
        }
        ) 
        res.status(201).json({
            error:false,
            message:"Successfully Saved",
            data:courser
        })
    } catch (error:any) {
        if(error.name == 'ValidationError'){
            const message=error.message.toString().replace(/[^a-zA-Z ]/g, "")
           return  res.status(422).json({
                err:true,
                message:"Error in saving",
                err_message:message,
            })
        }

          res.status(500).json({
            err:true,
            message:"Error in registering",
            // errors:`Required Fields are : ${error.details[0].path}`
        })
    
    }

}
//rigeter use with course
export const getCourse=async(req:Request,res:Response,next:NextFunction)=>{
    const limit =Number(req.query.limit) || 5;
    const offset =Number(req.query.offset) -1 || 0;
    let count: number;
    const skip=limit * offset;
    const search:string|any|undefined=req.query.search || ""
    try {
        const courser=await prisma.course.findMany({
            where:{
                name:{
                    contains:search
                }
            },
            skip:skip,
            take:limit,
        }
        )

        if(search !== ""){
                count=await prisma.course.count({
                    where:{
                        name:{
                            contains:search
                        }
                    }
                })
            }
            else{
            count=await prisma.course.count()
        }
        //aggregate count

        res.status(200).json({
            error:false,
            message:"Successfully fetched",
            data:{
                count:count,
                courser,
            }
        })


    } catch (error) {
        res.status(500).json({
            err:true,
            message:"Something went wrong",
        })
        // errors:`Required Fields are : ${error.details[0].path}`
    }
}
//update the course
export const updateCourse=async(req:Request,res:Response,next:NextFunction)=>{
    const data=req.body;
    const idCourse=req.params.id;

    try {
        const courser=await prisma.course.update({
            where:{
                id:idCourse
            },
            data:data
        })

        res.status(200).json({
            error:false,
            message:"Successfully Updated",
            data:courser
        })
    } catch (error:any) {
        if(error.name == 'ValidationError'){
            const message=error.message.toString().replace(/[^a-zA-Z ]/g, "")
              return  res.status(422).json({
                err:true,
                message:"Error in updating",
                err_message:message,
            })
        }
        res.status(500).json({
            err:true,
            message:"Something went wrong",
        })
    }
}
//delete the course
export const deleteCourse=async(req:Request,res:Response,next:NextFunction)=>{
    try {
        const idCourse=req.params.id;
        const courser=await prisma.course.delete({
            where:{
                id:idCourse
            }
        })

        res.status(200).json({
            error:false,
            message:"Successfully Deleted",
        })
    } catch (error:any) {
        // console.log(error)
        res.status(500).json({
            err:true,
            message:"something went wrong"
        })
    }
}

export const getCourseById=async(req:any,res:any)=>{
    const courseId=req.params.id || ""
    try {
            if(courseId === ""){
                return res.status(404).json({
                    err:false,
                    message:"Please provide a valid course id"
                })
            }

            const course=await prisma.course.findUnique({
                where:{
                    id:courseId
                }
            })

            if(!course){
                return res.status(404).json({
                    err:true,
                    message:"No course is found !!!"
                })
            }

            res.status(200).json({
                err:false,
                message:"data is successfully fetched",
                data:course
            })

    } catch (error) {
        // console.log(error)
        res.status(500).json({
            err:true,
            message:"something went wrong !!!"
        })
    }
}

export default {saveCourse,getCourse,updateCourse,deleteCourse}