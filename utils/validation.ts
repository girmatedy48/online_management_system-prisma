// import Joi from 'joi'
import  Joi  from 'joi'

export const registerSchema=Joi.object({
    email:Joi.string().email().required(),
    password:Joi.string().required(),
    firstName:Joi.string().required(),
    lastName:Joi.string().required(),
    role:Joi.string().required(),
    profilePicture:Joi.string().required()
})

export const loginSchema=Joi.object({
    email:Joi.string().email().required(),
    password:Joi.string().required(),
})

export const registerCourseSchema=Joi.object({
    name:Joi.string().required(),
    courseDetails:Joi.string().required(),
})

export const courseEnrollment=Joi.object({
    role:Joi.string().required(),
    userId:Joi.string().required(),
    courseId:Joi.string().required()
})

export const testResultSchema=Joi.object({
    courseId:   Joi.string().required(),
    testid:  Joi.string().required(),
    teacherId:   Joi.string().required(),
    studentid:  Joi.string().required(),
})

export const testSchema=Joi.object({
    courseId:   Joi.string().required(),
    name:  Joi.string().required(),
    date:  Joi.string().required(),
})

export const deleteUserEnrollement=Joi.object({
    id:Joi.string().required(),
    courseId:Joi.string().required()
})
export const updateUserEnrollementValidation=Joi.object({
    userId:Joi.string().required(),
    courseId:Joi.string().required(),
    role:Joi.string().required()
})


export const createTestResultValidation=Joi.object({
    courseId:Joi.string().required(),
    studentEmail:Joi.string().required(),
    teacherEmail:Joi.string().required(),
    score:Joi.number().required(),
    testId:Joi.string().required(),
})