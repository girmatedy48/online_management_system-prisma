import { NextFunction, Request, Response } from 'express'
import * as jwt from 'jsonwebtoken'



export  const verifyToken =async(req:any,res:Response,next:NextFunction)=>{

    const secret:string|any=process.env.SECRET;  

try{
    if(req.headers.authorization === undefined){
        return res.status(401).json({
            err:true,
            message:"You are not authorized to access this resource"
        })
    }
     const token=req.headers.authorization.split(' ')[1]

        if(token){
            const decoded=jwt.verify(token, secret);
            req.user=decoded
            next()
        }
        else{
            res.status(401).json({
                error: true,
                message: 'No Error, Please login again'
            })
        }
    }
    catch(err:any){
        res.status(401).json({
            error: true,
            err:err.message,
            message: 'Something went wrong, Please login again'
    })
}
}


//Admin persmission
export const authorizeAdmin =async(req:any,res:Response,next:NextFunction)=>{
    verifyToken(req,res,()=>{
        const user=req.user.user.role
        if(user === 'MEMBERS'){
            next()
        }
        else{
            res.status(403).json({
                error: true,
            message: 'You are not authorized to access this route'
            })
        }
    })
}
export default {verifyToken,authorizeAdmin}
