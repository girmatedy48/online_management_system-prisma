import { Request } from 'express'
import multer,{FileFilterCallback} from 'multer'

//function for uploading profile image
const storage=multer.diskStorage({
    //destination folder
    destination:(
        req:Request,
        file:Express.Multer.File,
        cb:(error: Error | null, destination: string)=>void
    ):void=>{
        cb(null,'./uploads')
    },
    //for renaming the image profiles
    filename:(
        req:Request,
        file:Express.Multer.File,
        cb:(error: Error | null, filename: string)=>void
        ):void=>{
        cb(null,new Date().toISOString() + file.originalname)
    }
})
//validate the image files
const fileFilter=(
    request:Request,
    file:Express.Multer.File,
    cb:FileFilterCallback
    ):void=>{
    //pass those image having below extensions
    if(file.mimetype ==='image/png'|| file.mimetype==='image/jpeg' || file.mimetype==='image/jpg'){
        cb(null,true) //store the image inside in our uploads file
    }
    else{
        cb(null,false)
    }
}

export const profileImage=multer({
    storage:storage,
    fileFilter:fileFilter,
    dest:'uploads/',
    limits:{
        fileSize:1024 * 1024 * 5 //5mb
    },
    
})