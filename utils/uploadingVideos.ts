import { Request } from 'express'
import multer,{FileFilterCallback} from 'multer'

//function for uploading profile image
const storage=multer.diskStorage({
    //destination folder
    destination:(
        req:Request,
        file:Express.Multer.File,
        cb:(error: Error | null, destination: string)=>void 
        ):void=>{
        cb(null,'./videos')
    },
    //for renaming the image profiles
    filename:(
        req:Request,
        file:Express.Multer.File,
        cb:(error: Error | null, filename: string)=>void
        ):void=>{
        cb(null,new Date().toISOString() + file.originalname)
    }
})
//validate the image files
const fileFilter=(
    request:Request,
    file:Express.Multer.File,
    cb:FileFilterCallback
    ):void=>{
    cb(null,true)
}

export const videoPofile=multer({
    storage:storage,
    fileFilter:fileFilter,
    dest:'videos/',
    limits:{
        fileSize:1024 * 1024 * 10 //10mb  
    }
   
    
})